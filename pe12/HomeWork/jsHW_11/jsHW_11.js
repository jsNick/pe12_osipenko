
console.log('jsHW_11.js');

let oBtns = document.querySelectorAll('button.btn');
// console.log( 'oBtns.length = ' + oBtns.length );


addEventListener("keydown", function(event) {

    // console.log('event.keyCode = ' + event.keyCode + '|' + event.code + '|' + event.key );

    let oActiveBtn = document.querySelectorAll('button.btn.active')[0];

    if ( oActiveBtn !== undefined ){
        oActiveBtn.setAttribute('class','btn')
    }

    for (let oBtn of oBtns) {
        if(event.key.toLocaleUpperCase()===oBtn.innerText.toLocaleUpperCase()){
            // console.log('oBtn.innerText = ' + oBtn.innerText )
            oBtn.setAttribute('class','btn active');
            break;
        }
    }


});