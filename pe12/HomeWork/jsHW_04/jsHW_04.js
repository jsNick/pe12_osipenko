// Написать функцию createNewUser(),
function createNewUser(){

    // которая будет создавать объект newUser
    let newUser = {};

    // При вызове функция должна спросить у вызывающего имя и фамилию
    newUser.firstName = prompt('First Name:');
    newUser.lastName = prompt('Last Name:');

    newUser.getLogin = function(){
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    }

    // и возвращать объект newUser.
    return newUser;
}

let newUser;

// Создать пользователя с помощью функции createNewUser().
newUser = createNewUser();
console.log('newUser.firstName = "' + newUser.firstName + '"');
console.log('newUser.lastName = "' + newUser.lastName + '"');
//Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
console.log('newUser.getLogin = "' + newUser.getLogin() + '"');
