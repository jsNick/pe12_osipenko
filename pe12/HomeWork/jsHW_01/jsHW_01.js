// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
let strName='';
let numAge='';
let blnAnswer=undefined;
let numCount;

// Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
while(strName == '' || Number.isInteger(parseInt(strName))){
    strName = prompt('Your name:',strName);
    console.log('strName = "' + strName + '"');
}

//debugger

if ( strName !== null ){

    numCount=0;
    // Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
    while( !Number.isInteger(parseInt(numAge)) || parseInt(numAge) < 0 ){
        numCount++;
        numAge = prompt('Your age, ' + strName + ':',numAge);
        console.log('numCount: ' + numCount + '; numAge = "' + numAge +'"');
        if ( numAge == null ) {break;};
        //if ( numCount > 15 ) {break;};
    }
}

//   if empty: strName = ""; numAge = ""
//if canceled: strName = "null"; numAge = null

if ( strName!== null && numAge!== null ){

    if (numAge < 18){
        // Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
        alert('You are not allowed to visit this website');
    }
    else if(numAge>=18 && numAge<=22){

        // Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel.

        blnAnswer = confirm('Are you sure you want to continue?');

        if (blnAnswer){
            //Если пользователь нажал Ok, показать на экране сообщение: Welcome, + имя пользователя.
            alert('Welcome, ' + strName);
        }else{
            //Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
            alert('You are not allowed to visit this website');
        }
    }
    else if(numAge>22){
        //Если возраст больше 22 лет - показать на экране сообщение: Welcome, + имя пользователя.
        alert('Welcome, ' + strName);
    }

}
