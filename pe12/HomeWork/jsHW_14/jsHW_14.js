console.log('jsHW_14.js');

// let oTabs = document.querySelectorAll('li.tabs-title');
let oTabs = $('li.tabs-title');
console.log('oTabs.length = '+oTabs.length);

// let oContents = document.querySelectorAll('.tabs-content li');
let oContents = $('.tabs-content li');
// console.log('oContents.length = '+oContents.length);

// let oMenu = document.getElementById('oMenu');

$('#oMenu').on('click', function(event) {

    let target = event.target;

    // console.log('target.tagName = '+target.tagName);


    if (target.tagName != 'LI') return;

    let i;

    // console.log('target.innerText = '+target.innerText);

    // let oActiveTab = document.querySelectorAll('li.tabs-title.active')[0];
    // let oActiveContent = document.querySelectorAll('li.tabs-content.active')[0];

    const oActiveTab = $('li.tabs-title.active')[0];
    const oActiveContent = $('li.tabs-content.active')[0];

    oActiveTab.setAttribute('class','tabs-title');
    target.setAttribute('class','tabs-title active');

    i = -1;
    for (let oTab of oTabs) {
        i++;
        if(oTab.innerText===target.innerText){break}

    }

    // console.log('i = '+i);

    const oContent = oContents[i];

    if (oActiveContent.innerText != oContent.innerText){
        oContent.setAttribute('class','tabs-content active');
        oActiveContent.removeAttribute('class');
    }

});