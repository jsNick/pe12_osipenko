
console.log('jsHW_10.js');

const frmPass = document.getElementById('frmPass');


const lblPassEnter = document.getElementById('lblPassEnter');
// console.log('lblPassEnter.innerText = ' + lblPassEnter.innerText);

const fldPassEnter = lblPassEnter.getElementsByTagName('input')[0];
// console.log('fldPassEnter.value = ' + fldPassEnter.value);

const oIconPassEnter = lblPassEnter.getElementsByTagName('i')[0];
// console.log('oIconPassEnter.className = \'' + oIconPassEnter.className + '\'' );


const lblPassConfirm = document.getElementById('lblPassConfirm');
// console.log('lblPassConfirm.innerText = ' + lblPassConfirm.innerText);

const fldPassConfirm = lblPassConfirm.getElementsByTagName('input')[0];
// console.log('fldPassConfirm.value = ' + fldPassConfirm.value);

const oIconPassConfirm = lblPassConfirm.getElementsByTagName('i')[0];
// console.log('oIconPassConfirm.className = \'' + oIconPassConfirm.className + '\'' );

const btnConfirm = frmPass.getElementsByTagName('button')[0];
// console.log('btnConfirm.innerText = ' + btnConfirm.innerText);

// btnConfirm.addEventListener('click', function(){
//     fnCheckPassEnterConfirm();
// });

frmPass.addEventListener('submit',function (event) {
   fnCheckPassEnterConfirm(event);
});

let generateError = function (text) {
    let error = document.createElement('div');
    error.className = 'error';
    error.style.color = 'red';
    error.innerHTML = text;
    return error;
};

let error;

oIconPassEnter.addEventListener('click', function(){
    switch (this.className) {
        case 'fas fa-eye icon-password':
            this.className = 'fas fa-eye-slash icon-password';
            fldPassEnter.setAttribute('type','text');
            break;
        case 'fas fa-eye-slash icon-password':
            this.className = 'fas fa-eye icon-password';
            fldPassEnter.setAttribute('type','password');
            break;
    }
});

oIconPassConfirm.addEventListener('click', function(){
    switch (this.className) {
        case 'fas fa-eye icon-password':
            this.className = 'fas fa-eye-slash icon-password';
            fldPassConfirm.setAttribute('type','text');
            break;
        case 'fas fa-eye-slash icon-password':
            this.className = 'fas fa-eye icon-password';
            fldPassConfirm.setAttribute('type','password');
            break;
    }
});

function fnCheckPassEnterConfirm(event){
    // console.log('fnCheckPassEnterConfirm');
    if(fldPassEnter.value===''){
        alert('Нужно ввести пароль');
        event.preventDefault();
    }
    else if(fldPassConfirm.value===''){
        alert('Нужно подтвердить пароль');
        event.preventDefault();
    }
    else if(fldPassEnter.value===fldPassConfirm.value){

        if(error!==undefined){
            error.remove();
            error=undefined;
        }

        alert('You are welcome');
    }else{
        //alert('Нужно ввести одинаковые значения');
        if(error===undefined) {
            error = generateError('Нужно ввести одинаковые значения');
            lblPassConfirm.append(error);
            event.preventDefault();
        }
    }
}