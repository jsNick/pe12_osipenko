## Теоретический вопрос

1. Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

## Ответ

Экранирование - это замена управляющих символов в языке программирования на управляющие последовательности, известные как escape-последовательности.

В java script для экранирования используется символ `\ `.

Например,

alert('Известная фраза: "I\\'ll be back"');