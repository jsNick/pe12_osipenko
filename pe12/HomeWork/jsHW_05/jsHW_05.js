// Написать функцию createNewUser(),
function createNewUser(){

    // которая будет создавать объект newUser
    let newUser = {};

    // При вызове функция должна спросить у вызывающего имя и фамилию
    newUser.firstName = prompt('First Name:');
    newUser.lastName = prompt('Last Name:');

    //При вызове функция должна спросить у вызывающего дату рождения
    //(текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
    newUser.birthday = prompt('Birthday dd.mm.yyyy:');

    //Добавить в объект newUser метод getLogin(), который будет возвращать
    // первую букву имени пользователя, соединенную с фамилией пользователя,
    // все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
    newUser.getLogin = function(){
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    }

    //Создать метод getAge() который будет возвращать сколько пользователю лет.
    newUser.getAge = function ()
    {
        let today = new Date();
        let birthDate = new Date(this.birthday.substr(6,4)
                                  +'-'+this.birthday.substr(3,2)
                                  +'-'+this.birthday.substr(0,2));
        let age = today.getFullYear() - birthDate.getFullYear();
        return age;
    }

    //Создать метод getPassword(), который будет возвращать первую букву
    // имени пользователя в верхнем регистре, соединенную с фамилией
    // (в нижнем регистре) и годом рождения.
    // (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
    newUser.getPassword = function(){
        return this.firstName.charAt(0).toLocaleUpperCase()
             + this.lastName.toLowerCase()
             + this.birthday.substr(6,4);
    }

    // и возвращать объект newUser.
    return newUser;
}

let newUser;

// Создать пользователя с помощью функции createNewUser().
newUser = createNewUser();

//Вывести в консоль результат работы функции createNewUser(),
console.log('newUser.firstName = "' + newUser.firstName + '"');
console.log('newUser.lastName = "' + newUser.lastName + '"');
console.log('newUser.birthday = "' + newUser.birthday + '"');
console.log('newUser.getLogin() = "' + newUser.getLogin() + '"');

// а также функций getAge() и getPassword() созданного объекта.
console.log('newUser.getAge() = "' + newUser.getAge() + '"');
console.log('newUser.getPassword() = "' + newUser.getPassword() + '"');
