console.log('jsHW_08.js');

let form = document.getElementById('formWithValidation');

let priceTextField =document.getElementById('price');

function fnCreateSpanPrice(oTextField,strSpanText){

    let oDiv = document.createElement('div');
    let oSpan = document.createElement('span');
    let btnX = document.createElement('button');

    oDiv.className='pane';

    oSpan.className = 'strPrice';
    oSpan.innerText = strSpanText;
    oDiv.appendChild(oSpan);

    btnX.className = 'remove-button';
    btnX.innerHTML='&times;';

    btnX.onclick = function(){
      let oParent = this.parentNode;
      //При нажатии на Х - span с текстом и кнопка X должны быть удалены.
      oParent.remove();
      // Значение, введенное в поле ввода, обнуляется.
      oTextField.value = '';
    };

    oDiv.appendChild(btnX);

    return oDiv;
}

let generateError = function (text) {
    let error = document.createElement('div');
    error.className = 'error';
    error.style.color = 'red';
    error.innerHTML = text;
    return error;
};

let oPane;
let error;

price.addEventListener('focus', function(){

    //При фокусе на поле ввода - у него должна появиться рамка зеленого цвета.
    this.style.border = '2px solid green';

    if(error!=undefined){
        error.remove();
    }

    if(oPane!=undefined){
        oPane.remove();
    }

});

price.addEventListener('blur', function(){

    //При потере фокуса на поле ввода - рамка зеленого цвета пропадает.
    this.style.border = '1px solid grey';

    if(this.value===''){

    }
    else if (this.value >= 0) {
        if (form.querySelector('.pane') === null) {
            oPane = fnCreateSpanPrice(this, `Цена такая: ${this.value}`);
            this.parentElement.insertBefore(oPane, this);
        } else {
            oPane.querySelector('.strPrice').innerText = `Цена такая: ${this.value}`;
        }
    }
    else if (this.value < 0){

        //Если пользователь ввел число меньше 0 - при
        // потере фокуса подсвечивать поле ввода красной рамкой,
        this.style.border = '2px solid red';

        //под полем выводить фразу - Please enter correct price.
        // span со значением при этом не создается.
        error = generateError('Please enter correct price.');
        this.parentElement.append(error);

    }
    else{
        this.style.border = '2px solid red';
        error = generateError('Please enter correct price.');
        this.parentElement.append(error);
    }

});
