let result = 0;

for (let i = 0; i < 5; i++) {
    let name = prompt('Сышиш, как завут?');
    let num = +prompt('Сколько есть мелочи?');

    if (!num) {
        let res = confirm(name + ' тиха-тиха, ты приехал, семки есть?');

        if (res) {
            num = num + 10;
        }

        let boxer = confirm('Ты боксер?');

        if (boxer) {
            break;

            continue;
        }
    }

    result = result + num;

    console.log(name, ' скинул нам ', num, ' мелочей');
}

console.log('Заработал -> ', result);
