( function () {
    console.log('libStrings.js');
}
)();

function fnstrProperCase(str){
    let res = '';
    for(let i=1; i<=str.length; i++){
        res += str.charAt(i);
    }
    return str.charAt(0).toLocaleUpperCase() + res.toLocaleLowerCase();
}

function getSubString(str,beg=0,end=0) {
    if (end===0){
        return str.slice(beg);
    }else{
        return str.slice(beg,end);
    }
}

function fnstrGetSubStringFromChar(str,strChar) {
    // str.slice(start,end);
    // str.substr(from,length);
    // str.substring(start,end);

    let numPos = -1;

    for(let i=0; i<=str.length-1;i++){
        if(strChar===str.charAt(i)){
            numPos = i;
            break;
        }
    }

    if(numPos===-1){
        return '';
    }else{
        return str.slice(numPos);
    }

}

function fnstrGetSubStringFromSubString(str,strSubString) {
    let numPos = -1;
    numPos = str.indexOf(strSubString)
    if(numPos===-1){
        return '';
    }else{
        return str.slice(numPos);
    }

}