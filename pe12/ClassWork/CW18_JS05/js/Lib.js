
/*
 * @desc Func for sum num1 and num2
 * @param {Number} a
 * @param {Number} b
 * @return {Number}
 */
function sum(a,b) {
    return a + b;
}

/*
 * @desc Func for multiply num1 and num2
 * @param {Number} a
 * @param {Number} b
 * @return {Number}
 */
function mul(a,b) {
    if( isFinite(a) && isFinite(b) ){
        return a * b;
    }else {
        return 0;
    }
}


/*
 * @desc Func for Difference num1 and num2
 * @param {Number} a
 * @param {Number} b
 * @return {Number}
 */
function dif(a,b) {
    if( isFinite(a) && isFinite(b) ){
        return a - b;
    }else {
        return NaN;
    }
}


/*
 * @desc Func for Hockey Hits
 * @param {Number} amountHits
 * @param {Number} amountGoals
 */
function fnHockeyHits(amountHits,amountGoals) {
    if( isFinite(amountHits) && isFinite(amountGoals) ){
        return amountGoals / amountHits * 100;
    }else {
        return NaN;
    }
}


/**
 * @desc Подсчитывает кол-во вхождений числа в строку
 * @param {String} str
 * @param {Number} num
 * @return {Number}
 **/
function numInStr(str, num) {
    let counter = 0;
    if(!str){
        return 0;
    }else{
        for (let i = 0; i < str.length; i++) {
            let s = str.charAt(i);

            if (s !== ' ' && +s === num) {
                counter++;
            }
        }
        return counter;
    }
}