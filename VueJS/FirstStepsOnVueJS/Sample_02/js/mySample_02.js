
console.log('mySample_02.js');

//укажем vue на контейнер нашего приложения
new Vue({
    el: "#app",
    data: {

        //Вариант списка № 1. Упрощенный
        // bears:[
        //     'гризли',
        //     'белый',
        //     'бурый',
        //     'панда'
        // ]

        //Вариант списка № 2. Продвинутый
        bears: [
            {
                name: 'гризли',
                status: false
            },
            {
                name: 'белый',
                status: false
            },
            {
                name: 'бурый',
                status: false
            },
            {
                name: 'панда',
                status: true
            }
        ]



    }
});