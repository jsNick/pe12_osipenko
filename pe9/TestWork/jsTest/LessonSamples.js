console.log('LessonSamples.js');

function fnLn02Pg04(){

    let animal = { eats: true };
    let rabbit = { jumps: true };

    rabbit.__proto__ = animal;

    console.log('fnLn02Pg04:');
    console.log('rabbit.eats = ',rabbit.eats);
    console.log('rabbit.jumps = ',rabbit.jumps);

}

function fnLn02Pg06(){

    let animal = {
        eats: true,
        walk(){
            // alert("Animal walk");
            return "Animal walk";
        }
    };
    let rabbit = { jumps: true };

    rabbit.__proto__ = animal;

    console.log('fnLn02Pg06:');
    //method walk is taken from prototype
    console.log('rabbit.walk() = ',rabbit.walk());

}

function fnLn02Pg07(){

    let animal = {
        eats: true,
        walk(){
            // alert("Animal walk");
            return "Animal walk";
        }
    };

    let rabbit = {
        jumps: true,
        __proto__: animal
    };

    let longEar = {
        earLength: 10,
        __proto__: rabbit
    };


    console.log('fnLn02Pg07:');
    //method <walk> is taken from prototype chain
    console.log('longEar.walk() = ',longEar.walk());
    //property <jumps> from prototype chain
    console.log('longEar.jumps = ',longEar.jumps);

}

function fnLn02Pg08(){

    let animal = {
        eats: true,
        walk(){
            // alert("Animal walk");
            return "Animal walk";
        }
    };

    let rabbit = {
        jumps: true,
        __proto__: animal
    };

    rabbit.walk = function(){
      return "Rabbit! Bounce-bounce!"
    };

    console.log('fnLn02Pg08:');
    console.log('rabbit.walk() = ',rabbit.walk());

}

function fnLn02Pg09(){

    let animal = {
        eats: true,
        walk(){
            if(!this.isSleeping){
                return "Animal walk";
            }
        },
        sleep(){
            this.isSleeping = true;
        }
    };

    let rabbit = {
        name: "White Rabbit",
        __proto__: animal
    };

    rabbit.sleep();

    console.log('fnLn02Pg09:');
    console.log('rabbit.isSleeping = ',rabbit.isSleeping);
    console.log('animal.isSleeping = ',animal.isSleeping);

}

function fnLn02Pg12(){

    let animal = {
        eats: true
    };

    function fnRabbit(name){
        this.name = name;
    }

    fnRabbit.prototype = animal;

    let rabbit = new fnRabbit("White Rabbit");

    console.log('fnLn02Pg12:');
    console.log('rabbit.name = ',rabbit.name);
    console.log('rabbit.eats = ',rabbit.eats);

}

function fnLn02Pg19(){
    let dt = new Date();
    console.dir(dt);
}
