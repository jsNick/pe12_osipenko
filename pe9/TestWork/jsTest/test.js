console.log('test.js');

function fnT1Q00(){
    let person = ["Bob", "Marly"];
    let [name, surname] = person;

    console.log('fnT1Q01:');
    console.log('name: ', name);
    console.log('surname: ', surname);

}


function fnT1Q01(){
    let person = {name:"Bob", surname:"Marly"};
    let {name, surname} = person;

    console.log('fnT1Q01:');
    console.log('name: ', name);
    console.log('surname: ', surname);

}


function fnT1Q02(){
    let foods = ["apple", "banana", "orange", "pineapple"];
    let [, , order] = foods;

    console.log('order: ', order);

}





function fnT1Q03(){

    let users = [
        {name: "Joe", age: 42},
        {name: "Den", age: 31},
        {name: "Tom", age: 19},
    ];

    console.log('fnT1Q03:');

    //Err in for
    // for(var [name, age] of users)
    // for(let [name, age] of users)
    // for(let [name, age] in users)
    for(let {name, age} of users)
    {
        // alert(name, age);
        console.log(name, age);
    }

}














function fnT1Q04(){
    let {name: visibleName, age = 18} = {name: "Den", age: 31};
    let registeredUser = {name, visibleName, age};
    console.log(registeredUser);

}

function fnT1Q05(){
    let options = {
        title: "Menu",
        width: 100,
        height: 200
    };

    // let { title, width, height } = options;
    let { height, title, width } = options;

    console.log('fnT1Q01:');
    console.log('title: ', title);
    console.log('width: ', width);
    console.log('height: ', height);

}

function fnT1Q06(){
    let {name, isNew = true, role = "user"} = {name: "Den", role: "registered", isNew: false};

    function formUser({name, status = "new", role = "moderator"}) {
        return {name, role, status};
    }

    let registeredUser = formUser({name, isNew: "new", role, status: isNew});

    console.log('fnT1Q06');
    console.log(registeredUser);
}

function fnT1Q08(){

    //Error: test.js:80 Uncaught ReferenceError: saySomething is not defined
    // console.log( 'saySomething(): ', saySomething('saySomething'));

    const hello = function saySomething(txt) {return txt;};

    console.log( 'hello(): ', hello('hello'));
}


function fnT1Q11(){
    function createAdd() {
        let start = 1;

        return function(add) {
            return start += add;
        }
    }

    let add1 = createAdd();
    let add2 = createAdd();

    let result = add1(1) + add1(2) + add2(1);

    console.log('fnT1Q11');
    console.log(result);

}


function fnT1Q12(){

    const x = {};

    x.method = function() {
        this.name = "Jane Doe";
        const z = () => alert(this.name);
        z();
    };

    x.name = "John Doe";
    x.method();
}

function fnT2Q06(){
    let prototypeObj = {
        property: 1,
        setProperty: function(value) {
            this.property += value;
        }
    }

    let obj = {}
    obj.__proto__ = prototypeObj;

    obj.setProperty(2);

    console.log('fnT2Q06');
    console.log('prototypeObj.property = ',prototypeObj.property);
    console.log('obj.property = ',obj.property);

}

function fnT2Q08(){
    function Animal() {
        this.moveType = this.__proto__.moveType;
    }

    let Fish = {
        moveType: "swim"
    }

    let Rabbit = {
        moveType: "jump"
    }

    Animal.__proto__ = Fish;
    Animal.prototype = Rabbit;

    let theBrave = new Animal();

    console.log('fnT2Q08');
    console.log('theBrave.moveType = ',theBrave.moveType);
    console.log('Animal.moveType = ',Animal.moveType);

}
